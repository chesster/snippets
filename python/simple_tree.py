tree = lambda: collections.defaultdict(tree)
root = tree()
root['menu']['id'] = 'file'
root['menu']['value'] = 'File'
root['menu']['menuitems']['new']['value'] = 'New'
root['menu']['menuitems']['new']['onclick'] = 'new();'
# json.dumps(root, sort_keys=True, indent=4, separators=(',', ': '))
# {
#     "menu": {
#         "id": "file",
#         "menuitems": {
#             "new": {
#                 "onclick": "new();",
#                 "value": "New"
#             },
#         },
#         "value": "File"
#     }
# }
