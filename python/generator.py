g = (x ** 2 for x in xrange(10))
# >>> next(g)
# 0
# >>> next(g)
# 1
# >>> next(g)
# 4
# >>> next(g)
# 9
