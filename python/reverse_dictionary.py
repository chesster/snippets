dic = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
reverse_dict = dict(zip(dic.values(), dic.keys()))
# >>> {1: 'a', 2: 'b', 3: 'c', 4: 'd' }
