# -*- coding: utf-8 -*-
# /project/apps/__APPNAME_/management/commands
from django.core.management.base import BaseCommand
import warnings
warnings.filterwarnings("ignore")


class Command(BaseCommand):

    help = 'Command help'

    options = {
        'default': [
            {'name': 'option1', 'dest': 'option1',
             'default': 'default', 'help': 'some option1', },
        ],
    }

    def add_arguments(self, parser):
        for option_type in self.options:
            for option in self.options[option_type]:
                parser.add_argument(
                    '--%s' % option['name'], dest=option['dest'], default=option['default'], help=option['help'],)
        pass

    def handle(self, *args, **options):
        fill_options = lambda self_options, options: {option['dest']: options.get(
            option['dest'], option['default']) for option in self_options}
        get_default_options = fill_options(self.options['default'], options)
        self.run_command(**get_default_options)

    @classmethod
    def run_command(cls, option1=None):
        print option1
