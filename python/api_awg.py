import requests
import json

site_url = "http://localhost:8000/"
base_url = site_url + 'api/'
auth_url = base_url + "token/auth/"
payload = {'username': 'admin', 'password': 'test'}
headers = {"Content-Type": "application/json"}
token = requests.post(auth_url, headers=headers,
                      data=json.dumps(payload)).json()
headers['Authorization'] = "AWG " + token['token']
generate_url = base_url + "generate/"
payload = {
    "generate_for_days": 0, "week": 1, "day": 1, "gender": 1,
    "target": 2, "disc": 1, "body_type": 0,
    "hormone": 4, "level": 1, "iteration": 0,
    "training_days": 3, "generate_for_weeks": 1, "regenerate": False}

training_request = requests.post(generate_url, json=payload)
